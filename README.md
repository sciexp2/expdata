SciExp²-ExpData
===============

SciExp²-ExpData provides helper functions for easing the workflow of analyzing
the many data output files produced by experiments. The helper functions simply
aggregate the many per-experiment files into a single data structure that
contains all the experiment results with appropriate metadata to identify each
of the experiment results (e.g., using a pandas data frame).

It works best in combination with SciExp²-ExpDef, which can be used to define
many experiments based on parameter permutations.

You can find the documentation in:

  https://sciexp2-expdata.readthedocs.io


Copyright
=========

Copyright 2019-2020 Lluís Vilanova <llvilanovag@gmail.com>

Sciexp²-ExpData is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Sciexp²-ExpData is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.
